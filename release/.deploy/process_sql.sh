CONNECTION="postgresql://$USER:$PASSWORD@$HOST:$PORT/$DB"

FILES=$(/usr/bin/yq '.deploy[]' $RELEASE)
echo $FILES

for PATH in $FILES
do
   echo "Processing file $PATH"
   /usr/bin/psql $CONNECTION -f $PATH
done