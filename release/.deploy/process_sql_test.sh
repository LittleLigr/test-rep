export PATH=$PATH:/usr/bin

FILES=$(/usr/bin/yq '.deploy[]' $RELEASE)
echo $FILES

for FILE_PATH in $FILES
do
   echo "Test file $FILE_PATH"
   py -m sqlfluff lint $FILE_PATH --dialect postgres
done